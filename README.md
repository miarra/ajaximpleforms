# Quickstart
This extension add more form components to Yours forms.

## Instalation
#### Download
The best way to install `AJAXimple/forms` is using Composer:
```sh
$ composer require ajaximple/forms
```
#### Registering
You can enable the extension using your neon config:
```sh
extensions:
	AJAXimpleForms: AJAXimple\Forms\AJAXimpleFormsExtension
```
#### Injecting
You can simply inject factory in Your Presenters/Services:
```php
public function __construct(AJAXimple\Forms\FormFactory $formFactory)
{
    parent::__construct();
    ....
}
```
or You can create your own form using:
```php
public function createComponentMyForm()
{
    $form = new AJAXimple\Forms\Form();
    ...
    return $form;
}
```
## Conclusion
This extension requires Nette2.4 and it is property of Antonín Jehlář © 2020