<?php

namespace AJAXimple\Forms;

use AJAXimple\Forms\Form;

interface FormFactory
{
    /**
     * @return Form Form with added traits
     */
    public function create();
}
