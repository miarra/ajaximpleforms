<?php

namespace AJAXimple\Forms\Traits\RC;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\ArrayHash;
use Nette\Forms\Controls\BaseControl;
use Nette\Utils\Strings;

class InputRC extends TextInput {
    
    const 
        IS_RC = '\AJAXimple\Forms\Traits\RC\InputRC::rcValidator',
        REGEXP = '#^\s*(\d\d)(\d\d)(\d\d)[ /]*(\d\d\d)(\d?)\s*$#',
        ERROR_RC_WRONG_FORMAT = 'error_rc';

    public function __construct($label = null, ArrayHash $errorMessages = null)
    {
        parent::__construct($label);
        $this->setType('RC');
        $this->setRequired(FALSE);
        
        $this->setAttribute('placeholder', 'XXXXXX/XXXX');
        
        $this->addRule(
                self::IS_RC, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::ERROR_RC_WRONG_FORMAT) ? $errorMessages[self::ERROR_RC_WRONG_FORMAT] : 'Wrong format of RC.'));
        $this->addFilter([$this, 'outputFilter']);
        
    }
    
    public function outputFilter($input) {
        $retVal = $input;
        if(!Strings::contains($input, '/')){
            $retVal = substr_replace($input, '/', 6, 0);
        }
        
        return $retVal;
    }
    
    /** 
     * Verify format of RC
     * @param int $rc RC
     * @return bool True if RC is in correct format
     * 
     * @author David Grudl [2007]
     */
    public static function verifyRC($rc)
    {
        $matches = 0;
        
        if (!preg_match(self::REGEXP, $rc, $matches)) {
            return false;
        }

        list(, $year, $month, $day, $ext, $c) = $matches;
        
        $year += $year < 54 ? 2000 : 1900;
        
        if ($c === '') {
            if($ext == '000'){
                return false;
            }
            $year -= 100;
        } else {
            $mod = ($year . $month . $day . $ext . $c) % 11;
            if (!$mod) {
                return false;
            }
        }
        
        if ($month > 50) {
            $month -= 50;
        } 
        if ($month > 20 && $year > 2003) {
            $month -= 20;
        }
        
        if (!checkdate($month, $day, $year)) {
            return false;
        }

        return true;
    }
    
    /**
     * Check, if the value is valid ic code
     * @param Nette\Forms\Controls\BaseControl $item Control with value to check
     * @return bool True, if is valid, false otherwise
     */
    public static function rcValidator(BaseControl $item) {
        return self::verifyRC($item->getValue());
    }
}