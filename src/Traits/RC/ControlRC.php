<?php

namespace AJAXimple\Forms\Traits\RC;

use Nette\Utils\ArrayHash;

trait ControlRC {

    /**
     * Add input with Rodne Cislo (CZ only) verify and formatting
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @return InputRC Input with mobile filters and formatting
     */
    public function addRC($name, $label = null, ArrayHash $errorMessage = null)
    {
        return $this[$name] = new InputRC($label, $errorMessage);
    }
}