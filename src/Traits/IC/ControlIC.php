<?php

namespace AJAXimple\Forms\Traits\IC;

use Nette\Utils\ArrayHash;

trait ControlIC {
    
    /**
     * Add input with IC verify and formatting
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @param Nette\Utils\ArrayHash $errorMessages Error messages for input: {'length' => string $lengthMessage}
     * @return IC Input with zip filters and formatting
     */
    public function addIC($name, $label = null, ArrayHash $errorMessages = null)
    {
        return $this[$name] = new InputIC($label, $errorMessages);
    }
}