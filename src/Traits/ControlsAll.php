<?php

namespace AJAXimple\Forms\Traits;

trait ControlsAll {
    use Date\ControlDate;
    use DateTime\ControlDateTime;
    use RC\ControlRC;
    use Mobile\ControlMobilePhone;
    use Zip\ControlZipCZ;
    use CheckBoxListBits\ControlCheckBoxListBits;
    use MultiSelectBits\ControlMultiSelectBits;
    use Container\ControlContainer;
}