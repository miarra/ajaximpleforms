<?php

namespace AJAXimple\Forms\Traits\Date;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\Strings;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;

class InputDate extends TextInput {
    
    /** @var string Format of value to set */
    private $inFormat;

    const 
        DATE = '\AJAXimple\Forms\Traits\Date\InputDate::verifyDate',
        DATE_FORMAT = '\AJAXimple\Forms\Traits\Date\InputDate::verifyFormat',
        DATE_GREGORIAN = '\AJAXimple\Forms\Traits\Date\InputDate::verifyGregorian',
        DATE_WRONG = 'date',
        DATE_FORMAT_WRONG = 'format',
        DATE_GREGORIAN_WRONG = 'gregorian',
        IN_FORMAT = 'd.m.Y';
        

    public function __construct($label = null, ArrayHash $errorMessages = null)
    {
        parent::__construct($label);
        $this->setRequired(FALSE);
        
        
        $this->setType('text');
        $this->addRule(
                self::DATE_FORMAT, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::DATE_FORMAT_WRONG) ? $errorMessages[self::DATE_FORMAT_WRONG] : 'Wrong format of date.'));
        $this->addRule(
                self::DATE, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::DATE_WRONG) ? $errorMessages[self::DATE_WRONG] : 'Wrong date.'));
        $this->addRule(
                self::DATE_GREGORIAN, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::DATE_GREGORIAN_WRONG) ? $errorMessages[self::DATE_GREGORIAN_WRONG] : 'You can use only dates after 15.10.1582.'));
        $this->addFilter([$this, 'outputFilter']);
        
    }
    
    public function outputFilter($input) {
        if($input instanceof DateTime){return $input;}
        if(empty($input)){return NULL;}
        list($day, $month, $year) = explode('.', $input);
        return DateTime::fromParts($year, $month, $day, 0, 0, 0);
    }

    /** 
     * Verify format of Date
     * @param int $date Date
     * @return bool True if Date is in correct format, false otherwise
     */
    public static function verifyDate($input){
        if(($date = $input->value) instanceof DateTime){return TRUE;}
        if(empty($date)) {return TRUE;}
        list($day, $month, $year) = explode('.', $date);
        return checkdate($month, $day, $year);
    }
    /** 
     * Verify format of Date
     * @param int $date Date
     * @return bool True if Date is in correct format, false otherwise
     */
    
    public static function verifyFormat($input){
        if(($date = $input->value) instanceof DateTime){return TRUE;}
        if(empty($date)) {return TRUE;}
        $values = explode('.', $date);
        if(count($values) != 3) {return FALSE;}
        return (Strings::length($values[0]) == 2 || Strings::length($values[1]) == 2 || Strings::length($values[2]) == 4);
    }
    
    /** 
     * Verify format of Date
     * @param int $date Date
     * @return bool True if Date is in correct format, false otherwise
     */
    
    public static function verifyGregorian($input){
        if(!($date = $input->value) instanceof DateTime){
            if(empty($date)) {return TRUE;}
            list($day, $month, $year) = explode('.', $date);
            $date = DateTime::fromParts($year, $month, $day, 0, 0, 0);
        }
        return $date > DateTime::fromParts(1582, 10, 14, 0, 0, 0);
    }
    
    public function setValue($date) {
        if($date instanceof DateTime || $date instanceof \DateTime){$date = $date->format(self::IN_FORMAT);}
        parent::setValue($date);
    }
    
    public function setDefaultValue($date) {
        if($date instanceof DateTime || $date instanceof \DateTime){$date = $date->format(self::IN_FORMAT);}
        parent::setDefaultValue($date);
    }
    
    public function getValue() {
        $value = parent::getValue();
        if (empty($value)) {return NULL;}
        return DateTime::createFromFormat(self::IN_FORMAT, $value);
    }
}