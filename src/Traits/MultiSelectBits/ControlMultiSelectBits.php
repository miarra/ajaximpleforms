<?php

namespace AJAXimple\Forms\Traits\MultiSelectBits;

use Nette\Utils\ArrayHash;

trait ControlMultiSelectBits {

    /**
     * Add input with MultiSelect which has nice formatted input and output
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @param string $errorMessage Error message for wrong format
     * @return InputMultiSelectBits Input with MultiSelect which input and output is formatted by bit mask (so it's int)
     */
    public function addMultiSelectBits($name, $label = null, $items = null, ArrayHash $errorMessages = null)
    {
        return $this[$name] = new InputMultiSelectBits($label, $items, $errorMessages);
    }
}