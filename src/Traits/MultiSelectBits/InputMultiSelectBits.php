<?php

namespace AJAXimple\Forms\Traits\MultiSelectBits;

use Nette\Forms\Controls\MultiSelectBox;
use Nette\Utils\ArrayHash;

class InputMultiSelectBits extends MultiSelectBox {
    
    public function __construct($label = null, $items = null, ArrayHash $errorMessages = null)
    {
        parent::__construct($label, $items);
        $this->setRequired(FALSE);
    }
    
    public function getValue() {
        $prom = parent::getValue();
        $retVal = 0;
        foreach ($prom as $value) {
            $retVal += (1 << $value);
        }
        return $retVal;
    }
    
    public function setValue($value) {
        if (!is_array($value)) {
            $mask = 1;
            $tmp = [];
            $i = 0;
            while ($value > 0) {
                if ($value & $mask) {
                    $tmp[] = $i;
                }
                $value = $value >> 1;
                $i++;
            }
            $value = $tmp;
        }
        parent::setValue($value);
    }
}