<?php

namespace AJAXimple\Forms\Traits\DateTime;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;

class InputDateTime extends TextInput {
    
    /** @var string Format of value to set */
    private $inFormat;

    const 
        DATE = '\AJAXimple\Forms\Traits\DateTime\InputDateTime::verifyDate',
        DATE_FORMAT = '\AJAXimple\Forms\Traits\DateTime\InputDateTime::verifyFormat',
        DATE_GREGORIAN = '\AJAXimple\Forms\Traits\DateTime\InputDateTime::verifyGregorian',
        DATE_WRONG = 'date',
        DATE_FORMAT_WRONG = 'format',
        DATE_GREGORIAN_WRONG = 'gregorian',
        IN_FORMAT = 'd.m.Y H:i';
        

    public function __construct($label = null, ArrayHash $errorMessages = null)
    {
        parent::__construct($label);
        $this->setRequired(FALSE);
        
        
        $this->setType('text');
        $this->addRule(
                self::DATE_FORMAT, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::DATE_FORMAT_WRONG) ? $errorMessages[self::DATE_FORMAT_WRONG] : 'Wrong format of date.'));
        $this->addRule(
                self::DATE, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::DATE_WRONG) ? $errorMessages[self::DATE_WRONG] : 'Wrong date.'));
        $this->addRule(
                self::DATE_GREGORIAN, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::DATE_GREGORIAN_WRONG) ? $errorMessages[self::DATE_GREGORIAN_WRONG] : 'You can use only dates after 15.10.1582.'));
        $this->addFilter([$this, 'outputFilter']);
        
    }
    
    public function outputFilter($input) {
        if($input instanceof DateTime){return $input;}
        if(empty($input)){return NULL;}
        list($date, $time) = explode(' ', $input);
        list($day, $month, $year) = explode('.', $date);
        list($hour, $minute) = explode(':', $time);
        return DateTime::fromParts($year, $month, $day, $hour, $minute);
    }

    /** 
     * Verify format of Date
     * @param int $date Date
     * @return bool True if Date is in correct format, false otherwise
     */
    public static function verifyDate($input){
        if(($dateTime = $input->value) instanceof DateTime){return TRUE;}
        if(empty($dateTime)){return TRUE;}
        list($date, $time) = explode(' ', $dateTime);
        list($day, $month, $year) = explode('.', $date);
        list($hour, $minute) = explode(':', $time);
        try {
            return DateTime::fromParts($year, $month, $day, $hour, $minute);
        } catch (\Nette\InvalidArgumentException $ex) {
            return FALSE;
        }
    }
    /** 
     * Verify format of Date
     * @param int $date Date
     * @return bool True if Date is in correct format, false otherwise
     */
    
    public static function verifyFormat($input){
        if(($date = $input->value) instanceof DateTime){return TRUE;}
        if(empty($date)){return TRUE;}
        return DateTime::createFromFormat(self::IN_FORMAT, $date);
    }
    
    /** 
     * Verify format of Date
     * @param int $date Date
     * @return bool True if Date is in correct format, false otherwise
     */
    
    public static function verifyGregorian($input){
        if(!($date = $input->value) instanceof DateTime){
            if(empty($date)){return TRUE;}
            $date = DateTime::createFromFormat(self::IN_FORMAT, $date);
//            list($day, $month, $year) = explode('.', $date);
//            $date = DateTime::fromParts($year, $month, $day, 0, 0, 0);
        }
        return $date > DateTime::fromParts(1582, 10, 14, 0, 0, 0);
    }
    
    public function setValue($date) {
        if($date instanceof DateTime || $date instanceof \DateTime){$date = $date->format(self::IN_FORMAT);}
        parent::setValue($date);
    }
    
    public function setDefaultValue($date) {
        if($date instanceof DateTime || $date instanceof \DateTime){$date = $date->format(self::IN_FORMAT);}
        parent::setDefaultValue($date);
    }
    
    public function getValue() {
        $value = parent::getValue();
        if(empty($value)){return NULL;}
        return DateTime::createFromFormat(self::IN_FORMAT, $value);
    }
}