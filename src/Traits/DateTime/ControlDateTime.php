<?php

namespace AJAXimple\Forms\Traits\DateTime;

use Nette\Utils\ArrayHash;

trait ControlDateTime {

    /**
     * Add input with mobile phone verify and formatting
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @param string $dateFormat Format of the given date
     * @param string $errorMessage Error message for wrong format
     * @return InputDateTime Input with mobile filters and formatting
     */
    public function addDateTime($name, $label = null, ArrayHash $errorMessages = null)
    {
        return $this[$name] = new InputDateTime($label, $errorMessages);
    }
}