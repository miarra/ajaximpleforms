<?php

namespace AJAXimple\Forms\Traits\Mobile;

use Nette\Utils\ArrayHash;

trait ControlMobilePhone {
    
    /**
     * Add input with mobile phone verify and formatting
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @param string $country Country code for recognize number format.
     * @param Nette\Utils\ArrayHash $errorMessages Error messages for input: {'length' => string $lengthMessage}
     * @return InputMobilePhone Input with mobile filters and formatting
     */
    public function addMobile($name, $label = null, $country = 'CZ', ArrayHash $errorMessages = null)
    {
        return $this[$name] = new InputMobilePhone($label, $country, $errorMessages);
    }
}