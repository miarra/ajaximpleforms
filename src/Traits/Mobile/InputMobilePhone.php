<?php

namespace AJAXimple\Forms\Traits\Mobile;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\Strings;
use Nette\Utils\ArrayHash;
use Nette\Forms\Controls\BaseControl;

class InputMobilePhone extends TextInput {
    
    /*private $formatByCountry = [
        'AL' => [self::REGEXP => '/^\+355 4 \d{3} \d{4}$/',                                                 self::FORMAT => '+355 4 xxx xxxx'],     // +355 4 xxx xxxx
        'AD' => [self::REGEXP => '/^\+376 \d{3} \d{3}$/',                                                   self::FORMAT => '+376 xxx xxxx'],       // +376 xxx xxxx
        'AT' => [self::REGEXP => '/^\+43 ([0-9]|( ))*$/',                                                    self::FORMAT => '+43 '],                // +43 .....
        'BY' => [self::REGEXP => '/^\+375 (\d{2} \d{3} \d{4})|(\d{3} \d{3} \d{3})|(\d{4} \d{2} \d{3})$/',   self::FORMAT => '+375 xx xxx xxxx'],    // +375 xx xxx xxxx || +375 xxx xxx xxx || +375 xxxx xx xxx
        'BE' => [self::REGEXP => '/^\+32 4\d{2}  \d{2} \d{2} \d{2}$/',                                      self::FORMAT => '+32 4xx  xx xx xx'],   // +32 4xx  xx xx xx
        'BA' => [self::REGEXP => '/^\+387 33 \d{3} \d{3}$/',                                                self::FORMAT => '+387 33 xxx xxx'],     // +387 33 xxx xxx
        'BG' => [self::REGEXP => '/^\+359 2 \d{3} \d{4}$/',                                                 self::FORMAT => '+359 2 xxx xxxx'],     // +359 2 xxx xxxx
        'CH' => [self::REGEXP => '/^\+41 7[5-9] \d{3} \d{2} \d{2}$/',                                       self::FORMAT => '+41 7x xxx xx xx'],    // +41 7{5,6,7,8,9} xxx xx xx
        'CY' => [self::REGEXP => '/^\+357 9\d \d{6}$/',                                                     self::FORMAT => '+357 9x xxxxxx'],      // +357 9x xxxxxx
        'CZ' => [self::REGEXP => '/^\+420 \d{3} \d{3} \d{3}$/',                                             self::FORMAT => '+420 xxx xxx xxx'],    // +420 xxx xxx xxx
        'DE' => [self::REGEXP => '/^\+49 01[5-7]x(x|)-xxxxxxx$/',                                           self::FORMAT => '+49 01xxx-xxxxxxx'],   // +49 015xx-xxxxxxx
        'DK' => [self::REGEXP => '/^\+45 \d{2}-\d{2}-\d{2}-\d{2}$/',                                        self::FORMAT => '+45 xx-xx-xx-xx'],     // +45 xx-xx-xx-xx
        'EE' => [self::REGEXP => '/^\+372 \d{4} \d{4}$/',                                                   self::FORMAT => '+372 xxxx xxxx'],      // +372 83xx xxxx
        'ES' => [self::REGEXP => '/^\+34 \d{3} \d{3} \d{3}$/',                                              self::FORMAT => '+34 xxx xxx xxx'],     // +34 xxx xxx xxx
        'FO' => [self::REGEXP => '/^\+298 \d{6}$/',                                                         self::FORMAT => '+298 xxxxxx'],         // +298 xxxxxx
        'FI' => [self::REGEXP => '/^\+358 9 \d{3} \d{3}$/',                                                 self::FORMAT => '+358 9 xxx xxx'],      // +358 9 xxx xxx
        'FR' => [self::REGEXP => '/^\+590 690 \d{2} \d{2} \d{2}$/',                                         self::FORMAT => '+590 690 xx xx xx'],   // +590 690 xx xx xx
        'GB' => [self::REGEXP => '/^\+44 \d{10}$/',                                                         self::FORMAT => '+44 7777666666'],      // +44 7777666666
        'GE' => [self::REGEXP => '/^\+995 - \d{3} - \d{3} \d{3}$/',                                         self::FORMAT => '+995 - xxx - xxx xxx'],// +995 - xxx - xxx xxx
        'GI' => [self::REGEXP => '/^\+350 \d{8}$/',                                                         self::FORMAT => '+350 20052200'],       // +350 20052200
        'GR' => [self::REGEXP => '/^\+30 6 9\d \d{7}$/',                                                    self::FORMAT => '+30 6 9x xxxxxxx'],    // +30 6 9x xxxxxxx
        'HU' => [self::REGEXP => '/^\+36 \d \d{3} \d{4}$/',                                                 self::FORMAT => '+36 x xxx xxxx'],      // +36 x xxx xxxx
        'HR' => [self::REGEXP => '/^\+385 \d{2} \d{3} \d{4}$/',                                             self::FORMAT => '+385 xx xxx xxxx'],    // +385 xx xxx xxxx
        'IE' => [self::REGEXP => '/^\+353 \d{2} \d{3} \d{4}$/',                                             self::FORMAT => '+353 xx xxx xxxx'],    // +353 xx xxx xxxx
        'IS' => [self::REGEXP => '/^\+354 \d{3} \d{4}$/',                                                   self::FORMAT => '+354 xxx xxxx'],       // +354 xxx xxxx
        'IT' => [self::REGEXP => '/^\+39 \d{3} \d{7}$/',                                                    self::FORMAT => '+39 xxx xxxxxxx'],     // +39 xxx xxxxxxx
        'LT' => [self::REGEXP => '/^\+370 \d \d{3} \d{4}$/',                                                self::FORMAT => '+370 x xxx xxxx'],     // +370 x xxx xxxx
        'LU' => [self::REGEXP => '/^\+352 6\d1 \d{3} \d{3}$/',                                              self::FORMAT => '+352 6x1 xxx xxx'],    // +352 6x1 xxx xxx
        'LV' => [self::REGEXP => '/^\+371 2\d \d{3} \d{3}$/',                                               self::FORMAT => '+371 2x xxx xxx'],     // +371 2x xxx xxx
        'MC' => [self::REGEXP => '/^\+377 6 \d{2} \d{2} \d{2} \d{2}$/',                                     self::FORMAT => '+377 6 xx xx xx xx'],  // +377 6 xx xx xx xx
        'MK' => [self::REGEXP => '/^\+389 7\d \d{6}$/',                                                     self::FORMAT => '+389 7x xxxxxx'],      // +389 7x xxxxxx 
        'MT' => [self::REGEXP => '/^\+356 \d{4} \d{4}$/',                                                   self::FORMAT => '+356 xxxx xxxx'],      // +356 xxxx xxxx
        'NO' => [self::REGEXP => '/^\+47 59\d \d{2} \d{3}$/',                                               self::FORMAT => '+47 59x xx xxx'],      // +47 59x xx xxx
        'NL' => [self::REGEXP => '/^\+31 \d \d{4} \d{4}$/',                                                 self::FORMAT => '+31 x xxxx xxxx'],     // +31 x xxxx xxxx
        'PL' => [self::REGEXP => '/^\+48 \d{3} \d{3} \d{3}$/',                                              self::FORMAT => '+48 xxx xxx xxx'],     // +48 xxx xxx xxx
        'PT' => [self::REGEXP => '/^\+351 9\d \d{3} \d{4}$/',                                               self::FORMAT => '+351 9x xxx xxxx'],    // +351 9x xxx xxxx
        'RO' => [self::REGEXP => '/^\+40 \d{3} \d{3} \d{3}$/',                                              self::FORMAT => '+40 xxx xxx xxx'],     // +40 xxx xxx xxx
        'RU' => [self::REGEXP => '/^\+7 \d{3} \d{3} \d{4}$/',                                               self::FORMAT => '+7 xxx xxx xxxx'],     // +7 xxx xxx xxxx
        'SE' => [self::REGEXP => '/^\+46 \d{3} \d{3} \d{3}$/',                                              self::FORMAT => '+46 xxx xxx xxx'],     // +46 xxx xxx xxx
        'SI' => [self::REGEXP => '/^\+386 1 \d{3} \d{2} \d{2}$/',                                           self::FORMAT => '+386 1 xxx xx xx'],    // +386 1 xxx xx xx
        'SK' => [self::REGEXP => '/^\+421 \d{3} \d{3} \d{3}$/',                                             self::FORMAT => '+421 xxx xxx xxx'],    // +421 xxx xxx xxx
        'SM' => [self::REGEXP => '/^\+378 \d{4} \d{3} \d{3}$/',                                             self::FORMAT => '+378 xxxx xxx xxx'],   // +378 xxxx xxx xxx
        'TR' => [self::REGEXP => '/^\+90 \d{3} \d{3} \d{4}$/',                                              self::FORMAT => '+90 xxx xxx xxxx'],    // +90 xxx xxx xxxx
        'UA' => [self::REGEXP => '/^\+380 \d{2} \d{3}-\d{2}-\d{2}$/',                                       self::FORMAT => '+380 xx xxx-xx-xx'],   // +380 xx xxx-xx-xx
        'VA' => [self::REGEXP => '/^\+39 06 \d{8}$/',                                                       self::FORMAT => '+39 06 xxxxxxxx'],     // +39 06 xxxxxxxx
    ];*/
    
    /** @var array Array with all european number format. Contains prefix, body and all original format. */
    private $formatByCountry = [
        'AL' => [self::REGEXP => [self::PREFIX => '/^\+355/',   self::NUMBER => '/^4\d{7}$/'],          self::FORMAT => '+355 4 xxx xxxx'],     // +355 4 xxx xxxx
        'AD' => [self::REGEXP => [self::PREFIX => '/^\+376/',   self::NUMBER => '/^\d{6}$/'],           self::FORMAT => '+376 xxx xxxx'],       // +376 xxx xxxx
        'AT' => [self::REGEXP => [self::PREFIX => '/^\+43/',    self::NUMBER => '/^\d*$/'],             self::FORMAT => '+43 '],                // +43 .....
        'BY' => [self::REGEXP => [self::PREFIX => '/^\+375/',   self::NUMBER => '/^\d{9}$/'],           self::FORMAT => '+375 xx xxx xxxx'],    // +375 xx xxx xxxx || +375 xxx xxx xxx || +375 xxxx xx xxx
        'BE' => [self::REGEXP => [self::PREFIX => '/^\+32/',    self::NUMBER => '/^4\d{8}$/'],          self::FORMAT => '+32 4xx  xx xx xx'],   // +32 4xx  xx xx xx
        'BA' => [self::REGEXP => [self::PREFIX => '/^\+387/',   self::NUMBER => '/^33\d{6}$/'],         self::FORMAT => '+387 33 xxx xxx'],     // +387 33 xxx xxx
        'BG' => [self::REGEXP => [self::PREFIX => '/^\+359/',   self::NUMBER => '/^2\d{7}$/'],          self::FORMAT => '+359 2 xxx xxxx'],     // +359 2 xxx xxxx
        'CH' => [self::REGEXP => [self::PREFIX => '/^\+41/',    self::NUMBER => '/^7[5-9]\d{7}$/'],     self::FORMAT => '+41 7x xxx xx xx'],    // +41 7{5,6,7,8,9} xxx xx xx
        'CY' => [self::REGEXP => [self::PREFIX => '/^\+357/',   self::NUMBER => '/^9\d{7}$/'],          self::FORMAT => '+357 9x xxxxxx'],      // +357 9x xxxxxx
        'CZ' => [self::REGEXP => [self::PREFIX => '/^\+420/',   self::NUMBER => '/^\d{9}$/'],           self::FORMAT => '+420 xxx xxx xxx'],    // +420 xxx xxx xxx
        'DE' => [self::REGEXP => [self::PREFIX => '/^\+49/',    self::NUMBER => '/^01[5-7]\d{9}$/'],    self::FORMAT => '+49 01xxx-xxxxxxx'],   // +49 015xx-xxxxxxx
        'DK' => [self::REGEXP => [self::PREFIX => '/^\+45/',    self::NUMBER => '/^\d{8}$/'],           self::FORMAT => '+45 xx-xx-xx-xx'],     // +45 xx-xx-xx-xx
        'EE' => [self::REGEXP => [self::PREFIX => '/^\+372/',   self::NUMBER => '/^\d{8}$/'],           self::FORMAT => '+372 xxxx xxxx'],      // +372 83xx xxxx
        'ES' => [self::REGEXP => [self::PREFIX => '/^\+34/',    self::NUMBER => '/^\d{9}$/'],           self::FORMAT => '+34 xxx xxx xxx'],     // +34 xxx xxx xxx
        'FO' => [self::REGEXP => [self::PREFIX => '/^\+298/',   self::NUMBER => '/^\d{6}$/'],           self::FORMAT => '+298 xxxxxx'],         // +298 xxxxxx
        'FI' => [self::REGEXP => [self::PREFIX => '/^\+358/',   self::NUMBER => '/^9\d{6}$/'],          self::FORMAT => '+358 9 xxx xxx'],      // +358 9 xxx xxx
        'FR' => [self::REGEXP => [self::PREFIX => '/^\+590/',   self::NUMBER => '/^690\d{6}$/'],        self::FORMAT => '+590 690 xx xx xx'],   // +590 690 xx xx xx
        'GB' => [self::REGEXP => [self::PREFIX => '/^\+44/',    self::NUMBER => '/^\d{10}$/'],          self::FORMAT => '+44 7777666666'],      // +44 7777666666
        'GE' => [self::REGEXP => [self::PREFIX => '/^\+995/',   self::NUMBER => '/^\d{9}$/'],           self::FORMAT => '+995 - xxx - xxx xxx'],// +995 - xxx - xxx xxx
        'GI' => [self::REGEXP => [self::PREFIX => '/^\+350/',   self::NUMBER => '/^\d{8}$/'],           self::FORMAT => '+350 20052200'],       // +350 20052200
        'GR' => [self::REGEXP => [self::PREFIX => '/^\+30/',    self::NUMBER => '/^69\d{8}$/'],         self::FORMAT => '+30 6 9x xxxxxxx'],    // +30 6 9x xxxxxxx
        'HU' => [self::REGEXP => [self::PREFIX => '/^\+36/',    self::NUMBER => '/^\d{8}$/'],           self::FORMAT => '+36 x xxx xxxx'],      // +36 x xxx xxxx
        'HR' => [self::REGEXP => [self::PREFIX => '/^\+385/',   self::NUMBER => '/^\d{9}$/'],           self::FORMAT => '+385 xx xxx xxxx'],    // +385 xx xxx xxxx
        'IE' => [self::REGEXP => [self::PREFIX => '/^\+353/',   self::NUMBER => '/^\d{9}$/'],           self::FORMAT => '+353 xx xxx xxxx'],    // +353 xx xxx xxxx
        'IS' => [self::REGEXP => [self::PREFIX => '/^\+354/',   self::NUMBER => '/^\d{7}$/'],           self::FORMAT => '+354 xxx xxxx'],       // +354 xxx xxxx
        'IT' => [self::REGEXP => [self::PREFIX => '/^\+39/',    self::NUMBER => '/^\d{10}$/'],          self::FORMAT => '+39 xxx xxxxxxx'],     // +39 xxx xxxxxxx
        'LT' => [self::REGEXP => [self::PREFIX => '/^\+370/',   self::NUMBER => '/^\d{8}$/'],           self::FORMAT => '+370 x xxx xxxx'],     // +370 x xxx xxxx
        'LU' => [self::REGEXP => [self::PREFIX => '/^\+352/',   self::NUMBER => '/^6\d1\d{6}$/'],       self::FORMAT => '+352 6x1 xxx xxx'],    // +352 6x1 xxx xxx
        'LV' => [self::REGEXP => [self::PREFIX => '/^\+371/',   self::NUMBER => '/^2\d{7}$/'],          self::FORMAT => '+371 2x xxx xxx'],     // +371 2x xxx xxx
        'MC' => [self::REGEXP => [self::PREFIX => '/^\+377/',   self::NUMBER => '/^6\d{8}$/'],          self::FORMAT => '+377 6 xx xx xx xx'],  // +377 6 xx xx xx xx
        'MK' => [self::REGEXP => [self::PREFIX => '/^\+389/',   self::NUMBER => '/^7\d{7}$/'],          self::FORMAT => '+389 7x xxxxxx'],      // +389 7x xxxxxx 
        'MT' => [self::REGEXP => [self::PREFIX => '/^\+356/',   self::NUMBER => '/^\d{8}$/'],           self::FORMAT => '+356 xxxx xxxx'],      // +356 xxxx xxxx
        'NO' => [self::REGEXP => [self::PREFIX => '/^\+47/',    self::NUMBER => '/^59\d{6}$/'],         self::FORMAT => '+47 59x xx xxx'],      // +47 59x xx xxx
        'NL' => [self::REGEXP => [self::PREFIX => '/^\+31/',    self::NUMBER => '/^\d{9}$/'],           self::FORMAT => '+31 x xxxx xxxx'],     // +31 x xxxx xxxx
        'PL' => [self::REGEXP => [self::PREFIX => '/^\+48/',    self::NUMBER => '/^\d{9}$/'],           self::FORMAT => '+48 xxx xxx xxx'],     // +48 xxx xxx xxx
        'PT' => [self::REGEXP => [self::PREFIX => '/^\+351/',   self::NUMBER => '/^9\d{8}$/'],          self::FORMAT => '+351 9x xxx xxxx'],    // +351 9x xxx xxxx
        'RO' => [self::REGEXP => [self::PREFIX => '/^\+40/',    self::NUMBER => '/^\d{9}$/'],           self::FORMAT => '+40 xxx xxx xxx'],     // +40 xxx xxx xxx
        'RU' => [self::REGEXP => [self::PREFIX => '/^\+7/',     self::NUMBER => '/^\d{10}$/'],          self::FORMAT => '+7 xxx xxx xxxx'],     // +7 xxx xxx xxxx
        'SE' => [self::REGEXP => [self::PREFIX => '/^\+46/',    self::NUMBER => '/^\d{9}$/'],           self::FORMAT => '+46 xxx xxx xxx'],     // +46 xxx xxx xxx
        'SI' => [self::REGEXP => [self::PREFIX => '/^\+386/',   self::NUMBER => '/^1\d{7}$/'],          self::FORMAT => '+386 1 xxx xx xx'],    // +386 1 xxx xx xx
        'SK' => [self::REGEXP => [self::PREFIX => '/^\+421/',   self::NUMBER => '/^\d{9}$/'],           self::FORMAT => '+421 xxx xxx xxx'],    // +421 xxx xxx xxx
        'SM' => [self::REGEXP => [self::PREFIX => '/^\+378/',   self::NUMBER => '/^\d{10}$/'],          self::FORMAT => '+378 xxxx xxx xxx'],   // +378 xxxx xxx xxx
        'TR' => [self::REGEXP => [self::PREFIX => '/^\+90/',    self::NUMBER => '/^\d{10}$/'],          self::FORMAT => '+90 xxx xxx xxxx'],    // +90 xxx xxx xxxx
        'UA' => [self::REGEXP => [self::PREFIX => '/^\+380/',   self::NUMBER => '/^\d{9}$/'],           self::FORMAT => '+380 xx xxx-xx-xx'],   // +380 xx xxx-xx-xx
        'VA' => [self::REGEXP => [self::PREFIX => '/^\+39/',    self::NUMBER => '/^06\d{8}$/'],         self::FORMAT => '+39 06 xxxxxxxx'],     // +39 06 xxxxxxxx
    ];
    
    /** @var string Country code for selected phone number */
    private $countryCode;

    const 
        IS_MOBILE_PHONE = '\AJAXimple\Forms\Traits\Mobile\InputMobilePhone::euMobileValidator',
        REGEXP = 'regexp',
        FORMAT = 'format',
        PREFIX = 'prefix',
        NUMBER = 'number',
        ERROR_EU_MOBILE_PHONE = 'eu_mobile';

    public function __construct($label = null, $country = 'CZ', ArrayHash $errorMessages = null)
    {
        parent::__construct($label);
        $this->setType('euMobile');
        $this->setRequired(FALSE);
        
        $this->countryCode = $country;
        $this->setAttribute('placeholder', Strings::upper($this->formatByCountry[$country][self::FORMAT]));
        
        $this->addRule(
                self::IS_MOBILE_PHONE, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::ERROR_EU_MOBILE_PHONE) ? $errorMessages[self::ERROR_EU_MOBILE_PHONE] : 'Wrong mobile number format. See placeholder.'),
                $this->formatByCountry[$this->countryCode]);
        
        $this->addFilter([$this, 'euMobileFormatFilter']);
    }
    
    /**
     * Remove all spaces and '-' from the string and return that string without country code
     * @param string $value Value to remove the prefix
     * @param string $prefix Prefix to remove
     * @return string Phone number without spaces, '-' and prefix
     */
    private static function __removePrefix($value, $prefix) {
        $value = Strings::replace($value, '/\s|-/');
        
        $prefix = Strings::match($value, $prefix);
        
        if ($prefix !== NULL) {
            $value = Strings::after($value, $prefix[0]);
        }
        
        return $value;
    }
    
    /** 
     * Turn mobile phone to specified format for selected country
     * @param string $value Value from input to change to national code
     * @return string Number in national format with country code
     */
    public function euMobileFormatFilter($value) {
        $prefix = $this->formatByCountry[$this->countryCode][self::REGEXP][self::PREFIX];
        $value = self::__removePrefix($value, $prefix);
        $format = $this->formatByCountry[$this->countryCode][self::FORMAT];
        
        $retVal = Strings::match($format, $prefix)[0];
        
        $format = Strings::after($format, $retVal);
        
        $j = 0;
        
        for($i = 0; $i < Strings::length($format); $i++) {
            if ($format[$i] != 'x') {
                $retVal .= $format[$i];
                continue;
            }
            $retVal .= $value[$j++];
        }
        
        return $retVal;
    }
    
    /**
     * Check, if the number is valid mobile phone in selected country
     * @param Nette\Forms\Controls\BaseControl $item Control with value to check
     * @param array $formatByCountry Contains array with regexp and text format for mobile pnohes from selected country
     * @return bool True, if is valid, false otherwise
     */
    public static function euMobileValidator(BaseControl $item, $formatByCountry) {
        return Strings::match(self::__removePrefix($item->getValue(), $formatByCountry[self::REGEXP][self::PREFIX]), $formatByCountry[self::REGEXP][self::NUMBER]) !== NULL;
    }
}