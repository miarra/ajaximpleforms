<?php

namespace AJAXimple\Forms\Traits\DIC;

use Nette\Utils\ArrayHash;

trait ControlDIC {
    
    /**
     * Add input with DIC verify and formatting
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @param Nette\Utils\ArrayHash $errorMessages Error messages for input: {'length' => string $lengthMessage}
     * @return DIC Input with zip filters and formatting
     */
    public function addDIC($name, $label = null, ArrayHash $errorMessages = null)
    {
        return $this[$name] = new InputDIC($label, $errorMessages);
    }
}