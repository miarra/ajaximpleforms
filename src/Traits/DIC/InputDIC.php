<?php

namespace AJAXimple\Forms\Traits\DIC;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\Strings;
use Nette\Utils\ArrayHash;
use Nette\Forms\Controls\BaseControl;

class InputDIC extends TextInput {
    
    const 
        IS_DIC = '\AJAXimple\Forms\Traits\DIC\InputDIC::dicValidator',
        REGEXP = '(^([a-zA-Z]){2}\d{8,10}$)',
        ERROR_DIC_WRONG_FORMAT = 'error_dic';

    public function __construct($label = null, ArrayHash $errorMessages = null)
    {
        parent::__construct($label);
        $this->setType('DIC');
        $this->setRequired(FALSE);
        
        $this->setAttribute('placeholder', 'AA00000000(00)');
        
        $this->addRule(
                self::IS_DIC, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::ERROR_DIC_WRONG_FORMAT) ? $errorMessages[self::ERROR_DIC_WRONG_FORMAT] : 'Wrong format of DIC.'));
        
        $this->addFilter([$this, 'dicFilter']);
    }

    public static function removeSpaces($value) {
        return Strings::upper(preg_replace('#\s+#', '', (string) $value));
    }
    /** 
     * Turn DIC to specified format for selected country
     * @param string $value Value from input to get correct format
     * @return string DIC in correct format
     */
    public function dicFilter($value) {
        return self::removeSpaces($value);
    }

    /**
     * Check, if the value is valid dic code
     * @param Nette\Forms\Controls\BaseControl $item Control with value to check
     * @return bool True, if is valid, false otherwise
     */
    public static function dicValidator(BaseControl $item) {
        return Strings::match(self::removeSpaces($item->getValue()), self::REGEXP) !== NULL;
    }
}