<?php

namespace AJAXimple\Forms\Traits\CheckBoxListBits;

use Nette\Forms\Controls\CheckboxList;
use Nette\Utils\ArrayHash;

class InputCheckBoxListBits extends CheckboxList {
    
    const 
        DATE = '\AJAXimple\Forms\Traits\Date\InputDate::verifyDate',
        DATE_FORMAT = '\AJAXimple\Forms\Traits\Date\InputDate::verifyFormat',
        DATE_GREGORIAN = '\AJAXimple\Forms\Traits\Date\InputDate::verifyGregorian',
        DATE_WRONG = 'date',
        DATE_FORMAT_WRONG = 'format',
        DATE_GREGORIAN_WRONG = 'gregorian',
        IN_FORMAT = 'd.m.Y';
        

    public function __construct($label = null, $items = null, ArrayHash $errorMessages = null)
    {
        parent::__construct($label, $items);
        $this->setRequired(FALSE);
    }
    
    public function getValue() {
        $prom = parent::getValue();
        $retVal = 0;
        foreach ($prom as $value) {
            $retVal += (1 << $value);
        }
        return $retVal;
    }
    
    public function setValue($value) {
        if (!is_array($value)) {
            $mask = 1;
            $tmp = [];
            $i = 0;
            while ($value > 0) {
                if ($value & $mask) {
                    $tmp[] = $i;
                }
                $value = $value >> 1;
                $i++;
            }
            $value = $tmp;
        }
        parent::setValue($value);
    }
}