<?php

namespace AJAXimple\Forms\Traits\CheckBoxListBits;

use Nette\Utils\ArrayHash;

trait ControlCheckBoxListBits {

    /**
     * Add input with checkBoxList which has nice formatted input and output
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @param string $errorMessage Error message for wrong format
     * @return InputCheckBoxListBits Input with checkBoxList which input and output is formatted by bit mask (so it's int)
     */
    public function addCheckBoxListBits($name, $label = null, $items = null, ArrayHash $errorMessages = null)
    {
        return $this[$name] = new InputCheckBoxListBits($label, $items, $errorMessages);
    }
}