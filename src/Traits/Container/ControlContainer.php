<?php

namespace AJAXimple\Forms\Traits\Container;

trait ControlContainer {

    /**
     * Add container to form
     * @param string $name Name of the container
     * @return InputContainer Container with applied traits
     */
    public function addContainer($name) {
        $control = new InputContainer;
        $control->currentGroup = $this->currentGroup;
        if ($this->currentGroup !== null) {
            $this->currentGroup->add($control);
        }
        return $this[$name] = $control;
    }
}