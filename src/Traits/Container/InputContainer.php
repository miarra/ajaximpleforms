<?php

namespace AJAXimple\Forms\Traits\Container;

use Nette\Forms\Container;
use AJAXimple\Forms\Traits\ControlsAll;

class InputContainer extends Container
{
    use ControlsAll;
}