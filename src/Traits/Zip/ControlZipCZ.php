<?php

namespace AJAXimple\Forms\Traits\Zip;

use Nette\Utils\ArrayHash;

trait ControlZipCZ {
    
    /**
     * Add input with zip verify and formatting
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @param Nette\Utils\ArrayHash $errorMessages Error messages for input: {'length' => string $lengthMessage}
     * @return ZipCZ Input with zip filters and formatting
     */
    public function addZipCZ($name, $label = null, ArrayHash $errorMessages = null)
    {
        return $this[$name] = new InputZipCZ($label, $errorMessages);
    }
}