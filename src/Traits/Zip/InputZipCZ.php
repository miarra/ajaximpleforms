<?php

namespace AJAXimple\Forms\Traits\Zip;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\Strings;
use Nette\Utils\ArrayHash;
use Nette\Forms\Controls\BaseControl;

class InputZipCZ extends TextInput {
    
    const 
        IS_ZIP = '\AJAXimple\Forms\Traits\Zip\InputZipCZ::zipValidator',
        REGEXP = '(^[1-7]\d{4}$)',
        ERROR_ZIP_WRONG_FORMAT = 'eu_mobile';

    public function __construct($label = null, ArrayHash $errorMessages = null)
    {
        parent::__construct($label);
        $this->setType('ZipCZ');
        $this->setRequired(FALSE);
        
        $this->setAttribute('placeholder', 'XXX XX');
        
        $this->addRule(
                self::IS_ZIP, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::ERROR_ZIP_WRONG_FORMAT) ? $errorMessages[self::ERROR_ZIP_WRONG_FORMAT] : 'Wrong zip format. See placeholder. X means number'));
        
        $this->addFilter([$this, 'zipFilter']);
    }
    
    public static function removeSpaces($value) {
        return preg_replace('#\s+#', '', (string) $value);
    }
    
    /** 
     * Turn zip to specified format for selected country
     * @param string $value Value from input to get correct format
     * @return string Zip in correct format
     */
    public function zipFilter($value) {
        $value = self::removeSpaces($value);
        return Strings::substring($value, 0, 3) . ' ' . Strings::substring($value, 3, Strings::length($value));
    }
    
    /**
     * Check, if the value is valid zip code
     * @param Nette\Forms\Controls\BaseControl $item Control with value to check
     * @return bool True, if is valid, false otherwise
     */
    public static function zipValidator(BaseControl $item) {
        return Strings::match(self::removeSpaces($item->getValue()), self::REGEXP) !== NULL;
    }
}