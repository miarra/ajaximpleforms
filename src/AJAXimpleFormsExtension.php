<?php

namespace AJAXimple\Forms;

use Nette\DI\CompilerExtension;

class AJAXimpleFormsExtension extends CompilerExtension
{
    public function loadConfiguration()
    {
//        $this->compiler->loadDefinitionsFromConfig(
//            $this->loadFromFile(__DIR__ . '/config/common.neon')['services']
//        ); // for nette 3.0
        
        $builder = $this->getContainerBuilder();
        
        // načtení konfiguračního souboru pro rozšíření
        $this->compiler->loadDefinitions(
                $builder,
                $this->loadFromFile(__DIR__ . '/config/common.neon')['services'],
                $this->name
        );
    }
}
