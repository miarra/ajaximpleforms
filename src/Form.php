<?php

namespace AJAXimple\Forms;

use Nette\Application\UI;

class Form extends UI\Form {
    use Traits\ControlsAll;
}
